const Tone = require('tone');

exports.interpret = (dls) => {
  dls.split('\n').forEach(cmdString => {
    let cmd = cmdString.split(' ');
    if (cmd[0] === 'play') {
      //create a synth and connect it to the master output (your speakers)
      var synth = new Tone.Synth().toMaster();
      //play a middle 'C' for the duration of an 8th note
      synth.triggerAttackRelease(cmd[1], cmd[2]);
    }
  })

}